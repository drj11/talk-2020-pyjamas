# Slides

see also script.md

## Slide 000

The one where David introduces himself.

- He is David Jones
- Member of the League of Movable Type
- Kerning Consultant

## Slide 010

The one where David explains what he has done, typographically speaking.

- RANALINA
- Link to Taylor Swift thread

## Slide 020 Lettering

The one where David explains the difference between lettering
and typography.

- Writing
- Lettering
- Typography

[Page 29 of "How to"]

## Slide 030 Typeface

The one where David explains the difference between
a typeface and a font.

- MS Comic Sans
- MS Comic Sans Italic
- MS Comic Sans Bold

## Slide 040 Family

The one where David explains what a superfamily is.

- Source Serif
- Source Sans
- Source Code

Each available in light/medium/regular/bold/and so on

## Slide 050 bezier

The one where David shows a cubic Bezier curve.

## Slide 055 Glyph

The one where David shows the structure of a big D.

## Slide 060 OpenType

The one where David explains what some OpenType features are.

Not just letters. Kerning is fun.

- TAY -kern-> TAY
- fly -liga-> fly

## Slide 062 exile

The one where David shows how Taylor Swift also likes ligatures.

## Slide 060 GSUB

The one where David explains a couple more OpenType features.

- 1989 -onum-> 1989
- Style -smcp-> Style

## Slide 075 feature

The one where David shows what a feature file looks like.

    languagesystem DFLT dflt;
    languagesystem latn dflt;

    @lowercase = [a    b    c];
    @smallcaps = [A.sc B.sc C.sc];

    feature smcp {
        sub @lowercase by @smallcaps;
    } smcp;


## Slide 300 Tools

The where David shows 3 popular font creation tools.

## Slide 920 python

The one where David explains how Python is incorporated into
larger systems.

## Slide 932 glue

The one where David shows how Python glues everything together.

## Slide 935 Extension

The one where David show some RoboFont extensions.

## Slide 950 further

The one where David glosses some reading material.

## Slide 970 Python

The one where David appeals to Kernighan and Pike's better sense.

## Slide 975 end

The one where David implores you to tear down the patriarchy.

## Slide 978 ice

The one where David asserts that there is hashtag no tech for ice.

## Elements of Python

abstraction: functions

iteration: for loops

encapsulation: classes and modules


