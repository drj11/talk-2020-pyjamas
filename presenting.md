# Notes for presenting

## REFERENCES

«Speaker Guide» sent to speakers at PyJamasConf 2020

## Notes

Desk

Front light, one or two Beurer daylight therapy lamp.
Pendant lamp. Reflector? What about daylight?

Microphone, on desk, pointing at face. Can be off to one side.

Laptop + screen

Tablet for speaker notes

headset?

Quiet room.

Curtains?

Timer. On Phone?
