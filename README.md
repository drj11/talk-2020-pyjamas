# Coding Typography

A short talk on Typography and Coding.

I gave this talk live online at PyJamasConf at 2020-12-05T08:30Z.

[The slides](talk-2020-pyjamas.pdf).

[The script](script.md) for once, is reasonably accurate.

## Blurb

The blurb I submitted for the proposal:

In _Coding Typography_ I will introduce you to some aspects of modern typography and how coding is helping a generation of type designers meet the challenges of 21st century typography. Python appears to be the language of choice for these people, and there is an opportunity for the worlds of type and the worlds of Python to meet and teach each other productively.

Typographers as a group are becoming more interested in coding, and when they look at coding, they increasingly look towards Python.
Nina Stössinger, a Senior Typeface Designer at Frere-Jones Type, teaches [Python Programming at the excellent Letterform Archive](https://letterformarchive.org/events/introduction-to-python-programming-for-the-type-designer).
In this quote she is talking about her scripting course, but I think the ideas apply more generally, and it is a nice thing to say about Python:

> you’ll likely find that programming can be a very useful superpower to pick up. The Python programming language is known for being especially straightforward to learn

Typography is the graphic design of type. _Type_ in this context meaning the shapes of letters that we use in our writing systems, and coding those shapes into font files.

This talk comes in 3 parts:

- Typography;
- Coding;
- Python.

In _Typography_ I will briefly introduce typography for beginners, and set the scene and direction for the rest of the talk.
In _Coding_ I will talk about how in the 20th century some typographers are using coding to help in their work.
In _Python_ I will outline some ways in which Python can be used directly and indirectly in helping people working with type; I'll introduce some of the tools type designers use such as Adobe Font Development Kit for OpenType, fonttools, and FontForge.

The talk is a prospectus, it's an illustration of what people are doing with type and Python now, and a taste of what some of things that might be possible in the future. You should leave with an idea of what type and Python can do together, and possibly some things you might want to investigate further if you're interested.

## REFERENCES

[LUPTON] Ellen Lupton, «Thinking with Type».

[HOWTO] Cristóbal Henestrosa, Laura Meseguer, José Scaglione,
  «How to create typefaces».

[LEMING] Tal Leming, «The Unofficial OpenType Cookbook».


## Further reading

«The Elements of Typographical Style», Robert Bringhurst.
Is an excellent introduction to typography.

«[http://designwithfontforge.com/en-US/index.html](Design With FontForge)»
is the manual for _FontForge_, but
contains a lot of useful material on font design generally,
and the relationships between letters.

«How to create typefaces―From sketch to screen»
really is about exactly what the title says.
It is very much focussed on the business of making typefaces.
Most typographers will probably never make a typeface, but
if that's your interest, this beautifully made little book is what you need.
It gets into the details of drawing, lettering, spacing, spaces, inking, contrast, digitisation, control points, curves, kerning tables, OpenType features, and even some aspects of managing clients and their expectations, and distribution and monetisation.
It's a joint work by three outstanding authors who are not afraid to let their personalities and opinions shine through.
Originally published in Spanish, and available in Polish, Portugeuse and possibly other languages too.
