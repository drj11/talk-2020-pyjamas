"""
folio.py

Add page numbers, folios, to the slides.
"""


# https://docs.python.org/3.8/library/pathlib.html
import pathlib

# https://docs.python.org/3.8/library/sys.html
import sys

# https://docs.python.org/3.8/library/xml.etree.elementtree.html
import xml.etree.ElementTree

# Placement of page number, in document units (mm) from top.
Y = 10


def main(argv=None):
    if argv is None:
        argv = sys.argv

    args = argv[1:]

    for arg in args:
        add_folio(arg)


def add_folio(path):
    """
    Modify the SVG file in place and add a page number, folio,
    text element.
    """

    Namespaces = dict(svg="http://www.w3.org/2000/svg")

    with open(path) as fd:
        doc = xml.etree.ElementTree.parse(fd)

    # folio as a string...
    folio = pathlib.PurePath(path).stem
    # ... and as an Element.
    folio_el = folio_element(folio)

    fs = doc.findall(".//svg:text[@id='folio']", Namespaces)
    if fs:
        # Change text on existing folio.
        f = fs[0]
        f.text = folio
    else:
        # add a text Element to layer1
        gs = doc.findall("svg:g", Namespaces)
        for g in gs:
            if g.attrib.get("id") == "layer1":
                g.append(folio_el)

    doc.write(path, encoding="utf-8", xml_declaration=True)


def folio_element(folio):
    frag_string = f"""<svg xmlns:svg="http://www.w3.org/2000/svg">
      <svg:text style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:6;line-height:1.25;font-family:'Andika New Basic';-inkscape-font-specification:'Andika New Basic';fill:#999999;fill-opacity:1;stroke:none;stroke-width:2"
    x="10"
    y="{Y}"
    id="folio">{folio}</svg:text>
    </svg>"""

    frag, = xml.etree.ElementTree.fromstring(frag_string)
    return frag


if __name__ == "__main__":
    main()
